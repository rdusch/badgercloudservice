﻿using BadgerCloudService.Library;
using BadgerCloudService.Models;
using BadgerCloudService.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BadgerCloudService.ViewModels
{
    public class PostViewModel
    {
        private ScanParser parser = new ScanParser();
        private string _ScanData;
        private string _QuestionList;

        [Required]
        public string Token { get; set; }
        [Required]
        public string ScanData
        {
            get
            {
                return this._ScanData;
            }
            set
            {
                this._ScanData = value;
                this.LeadLookUp = this.parser.ParseData(value);
            }
        }
        public string LocationAlias { get; set; }
        public Lookup LeadLookUp { get; private set; }
        public string Comments { get; set; }
        public bool IsUpdate { get; set; }
        public string QuestionList
        {
            get
            {
                return _QuestionList;
            }
            set
            {
                this._QuestionList = value;
                try
                {
                    this.QuestionListXml = QuestionListSerializer.DeserializeQuestions(value);
                }
                catch (Exception ex)
                {

                }
            }
        }
        public List<QuestionXml> QuestionListXml { get; private set; }
    }
}