﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BadgerCloudService.ViewModels
{
    public class FilterViewModel
    {
        private List<FilterViewModel> subFilters = new List<FilterViewModel>();

        public string Field { get; set; }
        public string Data { get; set; }
        public List<FilterViewModel> SubFilters { get { return this.subFilters; } set { this.subFilters = value; } }
    }
}