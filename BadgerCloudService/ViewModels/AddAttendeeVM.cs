﻿using BadgerCloudService.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BadgerCloudService.ViewModels
{
    public class AddAttendeeVM
    {
        [Required]
        public string Token { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string BadgeId { get; set; }
        public Lookup LookUp { get { return this.GetLookup(); } }

        private Lookup GetLookup()
        {
            return new Lookup() { BadgeId = this.BadgeId, LastName = this.LastName, EventCode = "IMTS0916" };
        }
    }
}