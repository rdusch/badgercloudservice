namespace BadgerCloudService.Migrations
{
    using BadgerCloudService.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BadgerCloudService.Models.Context.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BadgerCloudService.Models.Context.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //context.Events.AddOrUpdate(
            //    new Event()
            //    {
            //        Name = "IMTS2016",
            //        AuthKey = "92EF378E-AE73-46DB-9848-102ECBB2639E",
            //        ExhibitId = "1735750",
            //        EventCode = "IMTS0916",
            //        URl = "https://www.xpressleadpro.com/Leads/LeadsAPI/LeadsAPI.php"
            //    });

            //context.Machines.AddOrUpdate(
            //    new Machine()
            //    {
            //        Alias = "MULTUS",
            //        ModelNumber = "1234567890"

            //    },
            //    new Machine()
            //    {
            //        Alias = "GENOS",
            //        ModelNumber = "9876543210"

            //    });
        }
    }
}
