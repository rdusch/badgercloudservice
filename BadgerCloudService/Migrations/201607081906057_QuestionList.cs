namespace BadgerCloudService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class QuestionList : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Attendee", "QuestionList", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Attendee", "QuestionList");
        }
    }
}
