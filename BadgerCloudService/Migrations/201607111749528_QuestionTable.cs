namespace BadgerCloudService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class QuestionTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Answer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Attendee_Id = c.Int(nullable: false),
                        AttendeeAnswer = c.String(),
                        Question_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Attendee", t => t.Attendee_Id, cascadeDelete: true)
                .ForeignKey("dbo.StoredQuestion", t => t.Question_Id, cascadeDelete: true)
                .Index(t => t.Attendee_Id)
                .Index(t => t.Question_Id);
            
            CreateTable(
                "dbo.StoredQuestion",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Index = c.Int(nullable: false),
                        Question = c.String(),
                        Active = c.Boolean(nullable: false, defaultValue: true),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.Attendee", "QuestionList");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Attendee", "QuestionList", c => c.String());
            DropForeignKey("dbo.Answer", "Question_Id", "dbo.StoredQuestion");
            DropForeignKey("dbo.Answer", "Attendee_Id", "dbo.Attendee");
            DropIndex("dbo.Answer", new[] { "Question_Id" });
            DropIndex("dbo.Answer", new[] { "Attendee_Id" });
            DropTable("dbo.StoredQuestion");
            DropTable("dbo.Answer");
        }
    }
}
