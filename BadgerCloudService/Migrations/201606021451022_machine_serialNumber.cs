namespace BadgerCloudService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class machine_serialNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Machine", "SerialNumber", c => c.String());
            AlterColumn("dbo.Machine", "Alias", c => c.String(nullable: false));
            DropColumn("dbo.Machine", "MachineId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Machine", "MachineId", c => c.String());
            AlterColumn("dbo.Machine", "Alias", c => c.String());
            DropColumn("dbo.Machine", "SerialNumber");
        }
    }
}
