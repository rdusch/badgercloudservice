namespace BadgerCloudService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFlagForControlMachine : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Machine", "IsOnControl", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Machine", "IsOnControl");
        }
    }
}
