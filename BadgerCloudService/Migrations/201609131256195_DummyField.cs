namespace BadgerCloudService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DummyField : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DummyTable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Field = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DummyTable");
        }
    }
}
