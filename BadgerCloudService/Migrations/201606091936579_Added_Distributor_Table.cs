namespace BadgerCloudService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Distributor_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Distributor",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Branch = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Attendee", "Distributor_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Attendee", "Distributor_Id");
            AddForeignKey("dbo.Attendee", "Distributor_Id", "dbo.Distributor", "Id", cascadeDelete: true);
            DropColumn("dbo.Attendee", "Distributor");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Attendee", "Distributor", c => c.String());
            DropForeignKey("dbo.Attendee", "Distributor_Id", "dbo.Distributor");
            DropIndex("dbo.Attendee", new[] { "Distributor_Id" });
            DropColumn("dbo.Attendee", "Distributor_Id");
            DropTable("dbo.Distributor");
        }
    }
}
