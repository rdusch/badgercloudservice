﻿using BadgerCloudService.Objects;
using OfficeOpenXml;
using System.Collections.Generic;
using System.Linq;

namespace BadgerCloudService.Services.ReportService
{
    public class ExcelWriter : IReportService
    {
        public ExcelPackage CreateWorkBook(IEnumerable<Lead> list)
        {
            var package = new ExcelPackage();
            package.Workbook.Worksheets.Add("Leads").Cells["A1"].LoadFromCollection(list.ToList(), true);
            return package;
        }
    }
}