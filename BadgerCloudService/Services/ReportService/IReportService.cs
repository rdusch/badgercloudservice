﻿using BadgerCloudService.Objects;
using OfficeOpenXml;
using System.Collections.Generic;

namespace BadgerCloudService.Services.ReportService
{
    public interface IReportService
    {
        ExcelPackage CreateWorkBook(IEnumerable<Lead> list);
    }
}
