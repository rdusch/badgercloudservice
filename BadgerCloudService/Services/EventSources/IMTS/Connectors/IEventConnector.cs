﻿using BadgerCloudService.Models;
using BadgerCloudService.Objects;

namespace BadgerCloudService.Services.EventSources.IMTS.Connectors
{
    public interface IEventConnector
    {
        Attendee FindByBadgeIdAndLastName(Lookup lookup, Event auth);
    }
}
