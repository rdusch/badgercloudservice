﻿using BadgerCloudService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using BadgerCloudService.Objects;
using System.Net;
using System.IO;

namespace BadgerCloudService.Services.EventSources.IMTS.Connectors.IMTS2016
{
    public class IMTSConnection : IEventConnector
    {
        public Attendee FindByBadgeIdAndLastName(Lookup lookup, Event auth)
        {
            return this.GetDataFromService(lookup, auth, "GETBADGEinfo");
        }

        private Attendee GetDataFromService(Lookup lookup, Event auth, string function)
        {
            var Url = this.CreateUrl(lookup, auth, function);

            var request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = "GET";
            request.ContentType = "application/json";
            var response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            var json = reader.ReadToEnd();
            reader.Close();
            response.Close();
            var deserializedJson = ((Dictionary<string, Dictionary<string,string>>)(new JavaScriptSerializer()).Deserialize(json, typeof(Dictionary<string, Dictionary<string, string>>)));
            return this.ConvertJsonToAttendee(deserializedJson["Results"]);
        }

        private Attendee ConvertJsonToAttendee(Dictionary<string, string> rawAttendee)
        {
            Attendee attendee = new Attendee();

            attendee.FirstName = rawAttendee["First Name"];
            attendee.LastName = rawAttendee["Last Name"];
            attendee.Title = rawAttendee["Title"];
            attendee.Company = rawAttendee["Company"];
            attendee.Address = new Address()
            {
                AddressLine1 = rawAttendee["Street Address"],
                AddressLine2 = rawAttendee["Street Address 2"],
                City = rawAttendee["City"],
                State = rawAttendee["State / Province"],
                PostalCode = rawAttendee["Zipcode"],
                Country = rawAttendee["Country"].ToUpper(),
            };
            attendee.PhoneNumber = rawAttendee["Telephone"];
            attendee.Fax = rawAttendee["Fax"];
            attendee.Email = rawAttendee["Email"];
            attendee.RegistrationType = rawAttendee["Registration Type"];
            attendee.Division = rawAttendee["Division/Dept/District"];

            return attendee;
        }

        private string CreateUrl(Lookup lookup, Event auth, string function)
        {
            //var Url = "https://www.xpressleadpro.com/Leads/LeadsAPI/LeadsAPI.php";
            //var AuthKey = "92EF378E-AE73-46DB-9848-102ECBB2639E";
            //var ExhibitId = "1735750";
            //var EventCode = "IMTS0916";
            return auth.URl + "?AuthKey=" + auth.AuthKey + "&exid=" + auth.ExhibitId + "&eventcode=" + auth.EventCode + "&function=" + function + "&badge=" + lookup.BadgeId + "&lastname=" + lookup.LastName;
        }
    }
}
