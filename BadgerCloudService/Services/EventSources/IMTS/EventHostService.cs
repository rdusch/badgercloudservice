using BadgerCloudService.Models;
using BadgerCloudService.Objects;
using BadgerCloudService.Services.EventSources.IMTS.Connectors;
using System.Collections.Generic;

namespace BadgerCloudService.Services.EventSources.IMTS
{
    public class EventHostService : IEventService
    {
        private IEventConnector connector;

        public EventHostService()
        {
            connector = new Connectors.IMTS2016.IMTSConnection();
        }

        public Attendee LookUpEventParticipant(Lookup lookup, Event auth)
        {
            return this.connector.FindByBadgeIdAndLastName(lookup, auth);
        }
    }
}