using BadgerCloudService.Models;
using BadgerCloudService.Objects;
using System.Collections.Generic;

namespace BadgerCloudService.Services.EventSources
{
    public interface IEventService
    {
        Attendee LookUpEventParticipant(Lookup lookup, Event auth);
    }
}