using BadgerCloudService.Models;
using BadgerCloudService.Services.DistributorSources.Readers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BadgerCloudService.Services.DistributorSources
{
    public class DistributorList : IDistributorList
    {
        private IReader _repo;

        public DistributorList()
        {
            _repo = new ExcelReader();
        }

        public DistributorList(IReader reader)
        {
            _repo = reader;
        }

        public Distributor FindDistributor(Address address)
        {
            try
            {
                if (address.Country == "UNITED STATES" || address.Country == "CANADA")
                {
                    return this._repo.FindByPostalCode(address.PostalCode);
                }
                else
                {
                    return this._repo.FindByCountryCode(address.Country);
                }
            }
            catch (Exception ex)
            {
                return new Distributor(){ Name = "Unknown", Branch = "Unknown"};
            }
        }

        public List<string> GetDistributorNames()
        {
            return this._repo.GetAllDistributors();
        }

        public IEnumerable<string> GetDistributorBranches(string distributor)
        {
            IEnumerable<string> branches = new List<string>();

            if (this._repo.IsUsDistributor(distributor))
            {
                branches = this._repo.GetDistributorBranches(distributor).Distinct();
            }

            return branches;
        }
    }
}