using BadgerCloudService.Models;
using System.Collections.Generic;

namespace BadgerCloudService.Services.DistributorSources
{
    public interface IDistributorList
    {
        Distributor FindDistributor(Address address);

        List<string> GetDistributorNames();

        IEnumerable<string> GetDistributorBranches(string distributor);
    }
}