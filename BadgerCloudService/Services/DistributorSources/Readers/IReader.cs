﻿using BadgerCloudService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BadgerCloudService.Services.DistributorSources.Readers
{
    public interface IReader
    {
        Distributor FindByCountryCode(string CountryCode);

        Distributor FindByPostalCode(string PostalCode);

        List<string> GetAllDistributors();

        IEnumerable<string> GetDistributorBranches(string distributor);

        bool IsUsDistributor(string distributor);
    }
}
