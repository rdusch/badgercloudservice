﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using BadgerCloudService.Services.DistributorSources.DAL;
using BadgerCloudService.Models;

namespace BadgerCloudService.Services.DistributorSources.Readers
{
    public class ExcelReader :  IReader
    {
        public Distributor FindByCountryCode(string country)
        {
            var dataRows = this.ConvertToList(this.GetCountryRecord(country));

            var values = dataRows[0].ItemArray.ToList();

            Distributor distributor = new Distributor()
            {
                Name = values[2].ToString(),
                Branch = ""
            };

            return distributor;
        }

        public Distributor FindByPostalCode(string postalCode)
        {
            bool isCanada = this.IsCanada(postalCode);
            List<DataRow> dataRows;

            if (isCanada)
            {
                dataRows = this.ConvertToList(this.GetCanadaPostalCodeRecord(postalCode));
            }
            else
            {
                dataRows = this.ConvertToList(this.GetUsPostalCodesRecord(postalCode));
            }

            var values = dataRows[0].ItemArray.ToList();

            if (isCanada)
            {
                var province = this.ConvertToList(this.GetCanadaDistributorByProvince(values[1].ToString()))[0].ItemArray.ToList();
                Distributor distributor = new Distributor()
                {
                    Name = province[1].ToString(),
                    Branch = ""
                };

                return distributor;
            }
            else
            {
                Distributor distributor = new Distributor()
                {
                    Name = values[4].ToString(),
                    Branch = values[3].ToString()
                };

                return distributor;
            }
        }
        
        private DataSet GetCanadaDistributorByProvince(string province)
        {
            string whereClause = "WHERE PROVINCE = '" + province + "'";
            return this.GetDataSet(SheetIndex(ExcelSheet.CanadaDist), "Select *", whereClause);
        }

        private bool IsCanada(string postalCode)
        {
            if (postalCode.Length == 7)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private DataSet GetCanadaPostalCodeRecord(string postalCode)
        {
            string whereClause = "WHERE [Postal Code] = '" + postalCode.Substring(0,3) + "'";
            return this.GetDataSet(SheetIndex(ExcelSheet.CanadaZips), "SELECT *", whereClause);
        }

        private DataSet GetUsPostalCodesRecord(string postalCode)
        {
            string whereClause = "WHERE ZIP = '" + postalCode + "'";
            return this.GetDataSet(SheetIndex(ExcelSheet.US), "SELECT *", whereClause);
        }

        private DataSet GetCountryRecord(string country)
        {
            string whereClause = "WHERE COUNTRY = '" + country + "'";
            return this.GetDataSet(SheetIndex(ExcelSheet.Countries), "SELECT *", whereClause);
        }

        private List<DataRow> ConvertToList(DataSet ds)
        {
            var drs = ds.Tables[0].Rows;
            List<DataRow> records = new List<DataRow>();

            foreach (DataRow dr in drs)
            {
                records.Add(dr);
            }

            return records;
        }

        private DataSet GetDataSet(int excelSheet, string select, string whereClause)
        {
            var sheet = new DataSetConnection();

            return sheet.GetDataSet(excelSheet, select, whereClause);
        }

        public List<string> GetAllDistributors()
        {
            var list = this.GetUSDistributors();
            list.AddRange(this.GetCanadaDistributors());
            list.AddRange(this.GetAllOtherCountriesDistributors());
            list.Remove("");
            list.Remove("-");
            return list;
        }

        private List<string> GetAllOtherCountriesDistributors()
        {
            var drs = this.ConvertToList(this.GetDataSet(SheetIndex(ExcelSheet.Countries), "SELECT DISTINCT [DISTRIBUTOR]", ""));
            List<string> distributors = new List<string>();
            foreach (DataRow dr in drs)
            {
                distributors.Add(dr.ItemArray.ToList()[0].ToString());
            }

            return distributors;
        }

        private List<string> GetCanadaDistributors()
        {
            var drs = this.ConvertToList(this.GetDataSet(SheetIndex(ExcelSheet.CanadaDist), "SELECT DISTINCT [DISTRIBUTOR]", ""));
            List<string> distributors = new List<string>();
            foreach (DataRow dr in drs)
            {
                distributors.Add(dr.ItemArray.ToList()[0].ToString());
            }

            return distributors;
        }

        private List<string> GetUSDistributors()
        {
            var drs = this.ConvertToList(this.GetDataSet(SheetIndex(ExcelSheet.US), "SELECT DISTINCT [DISTRIBUTOR MAIN]", ""));
            List<string> distributors = new List<string>();
            foreach(DataRow dr in drs)
            {
                distributors.Add(dr.ItemArray.ToList()[0].ToString());
            }

            return distributors;
        }

        private int SheetIndex(ExcelSheet SheetName)
        {
            int sheetIndex = 0;
            switch (SheetName)
            {
                case ExcelSheet.Countries:
                    sheetIndex = 0;
                    break;
                case ExcelSheet.US:
                    sheetIndex = 5;
                    break;
                case ExcelSheet.CanadaZips:
                    sheetIndex = 1;
                    break;
                case ExcelSheet.CanadaDist:
                    sheetIndex = 2;
                    break;
            }

            return sheetIndex;
        }

        public IEnumerable<string> GetDistributorBranches(string distributor)
        {
            var drs = this.ConvertToList(this.GetDataSet(SheetIndex(ExcelSheet.US), "SELECT DISTINCT [DISTRIBUTOR]", "WHERE [DISTRIBUTOR MAIN] = '" + distributor + "'"));
            List<string> distributors = new List<string>();
            foreach (DataRow dr in drs)
            {
                distributors.Add(dr.ItemArray.ToList()[0].ToString());
            }

            return distributors;
        }

        public bool IsUsDistributor(string distributor)
        {
            var drs = this.ConvertToList(this.GetDataSet(SheetIndex(ExcelSheet.US), "SELECT DISTINCT [DISTRIBUTOR MAIN]", "WHERE [DISTRIBUTOR MAIN] = '" + distributor + "'"));
            if (drs.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
