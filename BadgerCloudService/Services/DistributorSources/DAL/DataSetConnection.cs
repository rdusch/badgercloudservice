﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Web;
using System.Threading.Tasks;

namespace BadgerCloudService.Services.DistributorSources.DAL
{
    internal class DataSetConnection
    {
        internal DataSet GetDataSet(int SheetIndex, string Select, string whereClause)
        {
            DataSet ds = new DataSet();
            string connectionString = this.GetConnectionString();

            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                conn.Open();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = conn;

                DataTable dtSheets = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                var Countries = dtSheets.Rows[SheetIndex];

                var tableName = Countries["TABLE_NAME"].ToString();

                cmd.CommandText = Select + " FROM [" + tableName + "] " + whereClause;

                DataTable dt = new DataTable();
                dt.TableName = tableName;
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                da.Fill(dt);

                ds.Tables.Add(dt);

                cmd = null;
            }

            return ds;
        }

        // Shameless Screen Grab from http://www.codeproject.com/Tips/705470/Read-and-Write-Excel-Documents-Using-OLEDB
        // It is a decent way to create a connection string. Worth noting for later.
        private string GetConnectionString()
        {
            Dictionary<string, string> props = new Dictionary<string, string>();
            // XLS - Excel 2003 and Older
            props["Provider"] = "Microsoft.Jet.OLEDB.4.0";
            props["Extended Properties"] = "Excel 8.0";
            var path = HttpContext.Current.Server.MapPath("~/App_Data/");
            var fileName = System.IO.Path.Combine(HttpContext.Current.Server.MapPath("~/App_Data/"), SettingConfig.GetSetting("DistributorFileName"));
            props["Data Source"] = fileName;

            StringBuilder sb = new StringBuilder();

            foreach (KeyValuePair<string, string> prop in props)
            {
                sb.Append(prop.Key);
                sb.Append('=');
                sb.Append(prop.Value);
                sb.Append(';');
            }

            return sb.ToString();
        }
    }

    internal enum ExcelSheet
    {
        Countries,
        US,
        CanadaZips,
        CanadaDist
    }
}
