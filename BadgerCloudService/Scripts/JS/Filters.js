﻿function Filter() {
    'use strict';
    this.Field = "";
    this.Data = "";
    this.SubFilters = [];
}

function hasfilters() {
    "use strict";
    if ($('#Filters').has("div").length) {
        return true;
    }
    else {
        return false;
    }
}

function createFilter(_this) {
    'use strict';
    var filter = new Filter();
    filter.Field = _this.id;
    filter.Data = $(_this).find('.data').val();
    if ($(_this).has('.subfilter')) {
        $(_this).children('.subfilter').each(function (index) {
            var _thisSub = this;
            filter.SubFilters[index] = createFilter(_thisSub);
        });
    }
    return filter;
}

function getFilters() {
    "use strict";

    var filters = [];
    $('.filter').each(function (index) {
        var _this = this;
        filters[index] = createFilter(_this);
    });
    return filters;
}

function filterExists(filterType) {
    "use strict";
    if ($('#Filters').has('#' + filterType).length) {
        return true;
    }
    else {
        return false;
    }
}

function addFilter(filter) {
    "use strict";

    if (!filterExists(filter)) {
        $.ajax({
            type: "POST",
            url: "/filters/getfilter",
            data: JSON.stringify({ Filter: filter }),
            contentType: "application/json",
            async: true,
            success: function (response) {
                $("#Filters").append(response);
            }
        });
    }
}

function addBranchFilter(distributor) {
    'use strict';
    var branchFilter = "BranchFilter";
    if (distributor === 'All') {
        $('#BranchFilter').children().remove();
    } else {
        $.ajax({
            type: "POST",
            url: "/filters/getfilter",
            data: JSON.stringify({ Filter: branchFilter, parameter: distributor }),
            contentType: "application/json",
            async: true,
            success: function (response) {
                if (filterExists(branchFilter)) {
                    $("#BranchFilter").replaceWith(response);
                } else {
                    $("#BranchFilter").append(response);
                }
            }
        });
    }
}

function removeFilter(filter) {
    "use strict";
    $('#' + filter).remove();
}

function ExportListToExcel() {
    "use strict";
    var filters = getFilters();
    $.ajax({
        type: "POST",
        url: "/reports/getexcelexport",
        data: JSON.stringify(filters),
        contentType: "application/json",
        async: true,
        success: function (response) {
            window.location = 'reports/Download?fileName=' + response;
        }
    });
}

function FilterLeadList() {
    "use strict";
    var filters = getFilters();
    $.ajax({
        type: "POST",
        url: "/Leads/getlist",
        data: JSON.stringify(filters),
        contentType: "application/json",
        async: true,
        success: function (response) {
            $("#LeadsList").replaceWith(response);
        }
    });
}