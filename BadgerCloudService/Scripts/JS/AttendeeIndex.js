﻿function searchByName() {
    "use strict";
    var data = $(".data").val();

    $.ajax({
        type: "POST",
        url: "/Attendees/search",
        data: JSON.stringify({Data: data}),
        contentType: "application/json",
        async: true,
        success: function (response) {
            $("#AttendeesTable").replaceWith(response);
        }
    });
}