﻿using BadgerCloudService.Models;
using BadgerCloudService.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BadgerCloudService.Library
{
    internal class EventDropDownContents
    {
        internal static List<SelectListItem> GetEventContents(IRepository repo)
        {
            List<SelectListItem> eventName = new List<SelectListItem>()
            {
                new SelectListItem(){ Text = "Select an Event", Value = "Select", Selected = true },
                new SelectListItem(){ Text = "All", Value = "All", Selected = false }
            };

            var events = repo.GetShowListings();
            foreach(Event eventObject in events)
            {
                eventName.Add(new SelectListItem() { Text = eventObject.Name, Value = eventObject.Name, Selected = false });
            }

            return eventName;
        }
    }
}