﻿using BadgerCloudService.Models;
using BadgerCloudService.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BadgerCloudService.Library
{
    internal class MachineDropDownListContents
    {
        internal static List<SelectListItem> GetMachineNameListContents(Models.DAL.IRepository repository)
        {
            List<SelectListItem> machineNames = new List<SelectListItem>()
            {
                new SelectListItem(){ Text = "Select a Machine", Value = "Select", Selected = true},
                new SelectListItem() { Text = "All", Value = "All", Selected = false}
            };

            var machines = repository.GetMachines();
            foreach (Machine machine in machines)
            {
                machineNames.Add(new SelectListItem() { Text = machine.Alias, Value = machine.Alias, Selected = false });
            }

            return machineNames;
        }
    }
}