﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BadgerCloudService.Library
{
    internal class DropDownContentsWrapper
    {
        internal static List<SelectListItem> GetList(IEnumerable<SelectListItem> selectItems, string selectMessage)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.AddRange(new List<SelectListItem>()
            {
                new SelectListItem() { Value = "Select", Text = selectMessage },
                new SelectListItem() { Value = "All", Text = "All" }
            });

            items.AddRange(selectItems);

            return items;
        }
    }
}