﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using BadgerCloudService.Models;
using BadgerCloudService.Objects;

namespace BadgerCloudService.Library
{
    internal class QuestionListSerializer
    {
        internal static List<QuestionXml> DeserializeQuestions(string questionString)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<QuestionXml>), new XmlRootAttribute("ArrayOfStoredQuestion"));
            StringReader reader = new StringReader(questionString);
            return (List<QuestionXml>)serializer.Deserialize(reader);
        }
    }
}