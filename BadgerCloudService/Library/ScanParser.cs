﻿using BadgerCloudService.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BadgerCloudService.Library
{
    public class ScanParser
    {
        public Lookup ParseData(string rawData)
        {
            var dataArray = this.SpiltString(rawData);

            if (dataArray.Length == 6)
            {
                return new Lookup()
                {
                    BadgeId = dataArray[0],
                    EventCode = dataArray[1],
                    FirstName = dataArray[2],
                    LastName = dataArray[3],
                    Company = dataArray[4]
                };
            }
            else
            {
                return new Lookup()
                {
                    BadgeId = "0"
                };
            }
        }

        private string[] SpiltString(string data)
        {
            return data.Split('$');
        }
    }
}