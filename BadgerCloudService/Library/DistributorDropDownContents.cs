﻿using BadgerCloudService.Services.DistributorSources;
using System.Collections.Generic;
using System.Web.Mvc;

namespace BadgerCloudService.Library
{
    internal class DistributorDropDownContents
    {
        internal static List<SelectListItem> GetDistributorNameList(IDistributorList repo)
        {
            List<string> stringNames = repo.GetDistributorNames();
            List<SelectListItem> listNames = new List<SelectListItem>();
            foreach (string name in stringNames)
            {
                var item = new SelectListItem()
                {
                    Value = name,
                    Text = name
                };
                listNames.Add(item);
            }

            return listNames;
        }

        internal static List<SelectListItem> GetDistributorBranchesByDistributor(IDistributorList repo, string distributor)
        {
            IEnumerable<string> branches = repo.GetDistributorBranches(distributor);
            List<SelectListItem> branchListItem = new List<SelectListItem>();
            foreach (string branch in branches)
            {
                var item = new SelectListItem()
                {
                    Value = branch,
                    Text = branch
                };
                branchListItem.Add(item);
            }

            return branchListItem;
        }
    }
}