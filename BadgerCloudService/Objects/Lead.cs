﻿namespace BadgerCloudService.Objects
{
    public class Lead
    {
        public int Id { get; set; }
        public string BadgeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
        public string Company { get; set; }
        public string Title { get; set; }
        public string Division { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Distributor { get; set; }
        public string DistributorBranch { get; set; }
        public string Machine { get; set; }
        public string Event { get; set; }
        public string ReportDate { get; set; }
        public int VisitCount { get; set; }
        public string Notes { get; set; }
    }
}