﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BadgerCloudService.Objects.Filters.Leads
{
    public class MachineFilter : IFilter<Lead>
    {
        public MachineFilter(string filterValue)
        {
            this.Data = filterValue;
            this.FilterType = "Machine";
        }

        public string FilterType { get; private set; }
        public string Data { get; private set; }

        public IEnumerable<Lead> Filter(IEnumerable<Lead> dataSet)
        {
            return dataSet.Where(l => l.Machine == this.Data);
        }

        public string _filterValue { get; set; }
    }
}