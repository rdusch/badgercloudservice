﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BadgerCloudService.Objects.Filters.Leads
{
    public class BranchSubFilter : IFilter<Lead>
    {
        public BranchSubFilter(string data)
        {
            this.Data = data;
            this.FilterType = "BranchSubFilter";
        }

        public IEnumerable<Lead> Filter(IEnumerable<Lead> dataSet)
        {
            if (this.Data == "All")
            {
                return dataSet;
            }
            else
            {
                return dataSet.Where(l => l.DistributorBranch == this.Data);
            }
        }

        public string FilterType
        {
            get;
            private set;
        }

        public string Data
        {
            get;
            private set;
        }
    }
}