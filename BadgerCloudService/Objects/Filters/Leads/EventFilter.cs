﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BadgerCloudService.Objects.Filters.Leads
{
    public class EventFilter : IFilter<Lead>
    {
        public EventFilter(string data)
        {
            this.FilterType = "Event";
            this.Data = data;
        }

        public string FilterType { get; private set; }
        public string Data { get; private set; }

        public IEnumerable<Lead> Filter(IEnumerable<Lead> dataSet)
        {
            return dataSet.Where(l => l.Event == this.Data);
        }
    }
}