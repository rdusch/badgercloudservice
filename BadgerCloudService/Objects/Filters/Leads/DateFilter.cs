﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace BadgerCloudService.Objects.Filters.Leads
{
    public class DateFilter : IFilter<Lead>
    {
        public DateFilter(string filterValue)
        {
            this.Data = filterValue;
            this.FilterType = "Date";
        }

        public string FilterType { get; private set; }
        public string Data { get; private set; }

        public IEnumerable<Lead> Filter(IEnumerable<Lead> dataSet)
        {
            throw new NotImplementedException();
        }
    }
}