﻿using System.Collections.Generic;
using System.Linq;

namespace BadgerCloudService.Objects.Filters.Leads
{
    public class DistributorFilter : IFilter<Lead>
    {
        private IEnumerable<IFilter<Lead>> SubFilters;

        public DistributorFilter(string filterValue, IEnumerable<IFilter<Lead>> subFilters)
        {
            this.SubFilters = subFilters;
            this.Data = filterValue;
            this.FilterType = "Distributor";
        }

        public string FilterType { get; private set; }
        public string Data { get; private set; }

        public IEnumerable<Lead> Filter(IEnumerable<Lead> dataSet)
        {
            dataSet = dataSet.Where(l => l.Distributor == this.Data);
            if (this.SubFilters.Count() > 0)
            {
                foreach (IFilter<Lead> subfilter in this.SubFilters)
                {
                    dataSet = subfilter.Filter(dataSet);
                }
            }

            return dataSet;
        }
    }
}