﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BadgerCloudService.Objects.Filters.Leads
{
    public class NameFilter : IFilter<Lead>
    {
        public NameFilter(string filterValue)
        {
            this.Data = filterValue;
            this.FilterType = "Name";
        }

        public string FilterType { get; private set; }
        public string Data { get; private set; }

        public IEnumerable<Lead> Filter(IEnumerable<Lead> dataSet)
        {
            return dataSet.Where(l => (l.FirstName.Contains(this.Data)) || (l.LastName.Contains(this.Data)) || ((l.FirstName + " " + l.LastName).Contains(this.Data)));
        }

    }
}