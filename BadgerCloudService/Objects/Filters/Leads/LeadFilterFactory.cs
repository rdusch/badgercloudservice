﻿using BadgerCloudService.ViewModels;
using System.Collections.Generic;

namespace BadgerCloudService.Objects.Filters.Leads
{
    public class LeadFilterFactory
    {
        public static List<IFilter<Lead>> CreateFilters(FilterViewModel[] filtersVm)
        {
            List<IFilter<Lead>> filters = new List<IFilter<Lead>>();
            if (filtersVm != null)
            {
                foreach (FilterViewModel filterVm in filtersVm)
                {
                    List<IFilter<Lead>> subFilters = new List<IFilter<Lead>>();
                    foreach (FilterViewModel subFilterVm in filterVm.SubFilters)
                    {
                        subFilters.Add(CreateSubFilter(subFilterVm));
                    }
                    filters.Add(CreateFilter(filterVm, subFilters));
                }
            }

            return filters;
        }

        // Only one Sub filter exists so I am only going to handle one.
        private static IFilter<Lead> CreateSubFilter(FilterViewModel subFilterVm)
        {
            return new BranchSubFilter(subFilterVm.Data);
        }

        private static IFilter<Lead> CreateFilter(FilterViewModel filterVM, List<IFilter<Lead>> subfilters)
        {
            IFilter<Lead> filter = null;

            switch (filterVM.Field)
            {
                case ("DateFilter"):
                    filter = new DateFilter(filterVM.Data);
                    break;
                case ("DistributorFilter"):
                    filter = new DistributorFilter(filterVM.Data, subfilters);
                    break;
                case ("MachineFilter"):
                    filter = new MachineFilter(filterVM.Data);
                    break;
                case ("NameFilter"):
                    filter = new NameFilter(filterVM.Data);
                    break;
                case ("EventFilter"):
                    filter = new EventFilter(filterVM.Data);
                    break;
            }

            return filter;
        }
    }
}