﻿using System.Collections.Generic;

namespace BadgerCloudService.Objects.Filters
{
    public interface IFilter<T>
    {
        IEnumerable<T> Filter(IEnumerable<T> dataSet);
        string FilterType { get; }
        string Data { get; }
    }
}
