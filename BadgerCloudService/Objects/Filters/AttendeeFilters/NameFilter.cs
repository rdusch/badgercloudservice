﻿using BadgerCloudService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BadgerCloudService.Objects.Filters.AttendeeFilters
{
    public class NameFilter : IFilter<Attendee>
    {
        public NameFilter(string data)
        {
            this.Data = data;
            this.FilterType = "NameFilter";
        }

        public IEnumerable<Attendee> Filter(IEnumerable<Attendee> dataSet)
        {
            return dataSet.Where(l => (l.FirstName.Contains(this.Data)) || (l.LastName.Contains(this.Data)) || ((l.FirstName + " " + l.LastName).Contains(this.Data)));
        }

        public string FilterType
        {
            get;
            private set;
        }

        public string Data
        {
            get;
            private set;
        }
    }
}