﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BadgerCloudService.Objects
{
    public class Lookup
    {
        public string BadgeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EventCode { get; set; }
        public string Company { get; set; }
    }
}