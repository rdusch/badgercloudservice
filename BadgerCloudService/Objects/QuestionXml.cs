﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace BadgerCloudService.Objects
{
    [XmlRoot("StoredQuestion")]
    [XmlType("StoredQuestion")]
    public class QuestionXml
    {
        public int Index { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}