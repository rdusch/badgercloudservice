﻿namespace BadgerCloudService.Objects
{
    using BadgerCloudService.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public class PopularMachine
    {
        public Machine Machine { get; set; }
        public int Count { get; set; }
    }
}