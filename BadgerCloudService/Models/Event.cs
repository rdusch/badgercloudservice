﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BadgerCloudService.Models
{
    public class Event
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Address Address { get; set; }
        public string AuthKey { get; set; }
        public string ExhibitId { get; set; }
        public string EventCode { get; set; }
        public string URl { get; set; }
    }
}