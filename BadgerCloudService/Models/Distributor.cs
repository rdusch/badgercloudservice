﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BadgerCloudService.Models
{
    public class Distributor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Branch { get; set; }
        public override string ToString()
        {
            var format = this.Name;
            if (this.Branch != null && this.Branch != string.Empty && this.Branch != "")
            {
                format += ", " + this.Branch;
            }

            return format;
        }
    }
}