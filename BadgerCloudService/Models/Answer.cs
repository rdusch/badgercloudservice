﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BadgerCloudService.Models
{
    public class Answer
    {
        public int Id { get; set; }
        public int Attendee_Id { get; set; }
        [ForeignKey("Attendee_Id")]
        public Attendee Attendee { get; set; }
        [Display(Name = "Answer")]
        public string AttendeeAnswer { get; set; }
        public int Question_Id { get; set; }
        [ForeignKey("Question_Id")]
        public StoredQuestion Question { get; set; }
    }
}