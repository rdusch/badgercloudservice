﻿using BadgerCloudService.Objects;
using BadgerCloudService.Objects.Filters;
using System.Collections.Generic;

namespace BadgerCloudService.Models.DAL
{
    public interface IRepository
    {
        Attendee AddAttendee(Attendee attendee);

        Attendee FindAttendee(int? id);

        Attendee FindAttendeeByBadgeId(string badgeId);

        Distributor AddDistributor(Distributor distributor);

        Distributor FindDistributor(Distributor distributor);

        Distributor FindDistributorById(int id);

        Event GetEventByEventCode(string eventCode);

        IEnumerable<Scan> GetFilteredScansByDistributorName(string distributorName);

        IEnumerable<Lead> GetLeadList();

        IEnumerable<Attendee> GetAttendees();

        IEnumerable<Scan> GetScans();

        IEnumerable<Event> GetShowListings();

        IEnumerable<Machine> GetMachines();

        IEnumerable<Lead> GetFilteredLeads(List<IFilter<Lead>> filters);

        IEnumerable<StoredQuestion> GetQuestions();

        Machine FindMachineByAlias(string machineAlias);

        Scan FindScanById(int? id);

        Scan AddScan(Attendee attendee, Machine machine);

        void UpdateAttendee(Attendee attendee);

        void Dispose();

        void UpdateScan(Scan scan);

        void RemoveAttendee(int id);

        StoredQuestion FindQuestionByIndex(int index);

        StoredQuestion AddQuestion(QuestionXml questionXml);

        void AddAnswersRange(List<Answer> answers);

        IEnumerable<Attendee> GetFilteredAttendees(IFilter<Attendee> filter);

        void AddDummyEntry(string field);

        IEnumerable<PopularMachine> GetPopularMachine(System.DateTime dateTime);

        int GetAttendeeCount(System.DateTime dateTime);
    }
}
