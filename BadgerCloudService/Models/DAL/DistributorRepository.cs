﻿using BadgerCloudService.Models.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BadgerCloudService.Models.DAL
{
    public class DistributorRepository : IDisposable
    {
        ~DistributorRepository()
        {
            Dispose();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public Distributor Add(Distributor distributor)
        {
            using (var context = ApplicationDbContext.Create())
            {
                context.Distributors.Add(distributor);
                context.SaveChanges();
                return distributor;
            }
        }

        public Distributor Find(int id)
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Distributors.Find(id);
            }
        }

        public Distributor FindByNameAndBranch(string name, string branch)
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Distributors.Where(d => d.Name == name && d.Branch == branch).SingleOrDefault();
            }
        }
    }
}