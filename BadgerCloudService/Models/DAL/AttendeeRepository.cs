﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using BadgerCloudService.Models;
using BadgerCloudService.Models.Context;

namespace BadgerCloudService.Models.DAL
{
    public sealed class AttendeeRepository : IDisposable
    {
        ~AttendeeRepository()
        {
            Dispose();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public void Add(Attendee attendee)
        {
            using (var context = ApplicationDbContext.Create())
            {
                context.Attendees.Add(attendee);
                context.SaveChanges();
            }
        }

        public Attendee Find(int? id)
        {
            using (var context = ApplicationDbContext.Create())
            {
                var attendee = context.Attendees
                    .Include(a => a.Address)
                    .Include(a => a.Distributor)
                    .SingleOrDefault(a => a.Id == id);

                attendee.QuestionList = context.Answers.Where(a => a.Attendee_Id == attendee.Id).Include(a => a.Question).ToList();

                return attendee;
            }
        }

        public Attendee FindByBadgeId(string badgeId)
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Attendees.AsNoTracking()
                    .Include(a => a.Address)
                    .Where(a => a.BadgeId == badgeId)
                    .SingleOrDefault();
            }
        }

        public IEnumerable<Attendee> GetAttendees()
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Attendees
                    .Include(a => a.Address)
                    .ToList();
            }
        }

        public void Update(Attendee attendee)
        {
            using (var context = ApplicationDbContext.Create())
            {
                context.Attendees.Attach(attendee);
                context.Entry(attendee).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Remove(Attendee attendee)
        {
            using (var context = ApplicationDbContext.Create())
            {
                context.Entry(attendee).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }

        internal int CountWithDate(DateTime startDate)
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Attendees.Where(a => a.DateAdded > startDate.Date).Count();
            }
        }
    }
}