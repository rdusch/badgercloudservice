﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using BadgerCloudService.Models;
using BadgerCloudService.Models.Context;

namespace BadgerCloudService.Models.DAL
{
    public sealed class EventRepository : IDisposable
    {
        ~EventRepository()
        {
            Dispose();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public void Add(Event _event)
        {
            using (var context = ApplicationDbContext.Create())
            {
                context.Events.Add(_event);
            }
        }

        public Event Find(int? id)
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Events.Find(id);
            }
        }

        public Event FindEventByEventCode(string eventCode)
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Events.AsNoTracking()
                    .Where(e => e.EventCode == eventCode)
                    .SingleOrDefault();
            }
        }

        public IEnumerable<Event> GetAllEvents()
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Events.ToList();
            }
        }
    }
}