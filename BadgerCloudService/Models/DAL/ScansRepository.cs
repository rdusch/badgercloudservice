﻿using BadgerCloudService.Models.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BadgerCloudService.Models.DAL
{
    public sealed class ScansRepository : IDisposable
    {
        ~ScansRepository()
        {
            Dispose();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public void Add(Scan scan)
        {
            using (var context = ApplicationDbContext.Create())
            {
                context.Scans.Add(scan);
                context.SaveChanges();
            }
        }

        public Scan Find(int? id)
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Scans
                    .Include(s => s.Attendee.Address)
                    .Include(s => s.Machine)
                    .SingleOrDefault(s => s.Id == id);
            }
        }

        public IEnumerable<Scan> GetAllScans()
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Scans
                    .Include(s => s.Attendee)
                    .Include(s => s.Machine)
                    .ToList();
            }
        }

        public IEnumerable<Scan> GetScansByDistributorName(string distributor)
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Scans
                    .Include(s => s.Attendee)
                    .Include(s => s.Machine)
                    .Where(s => s.Attendee.Distributor.Name == distributor)
                    .ToList();
            }
        }

        public void Update(Scan scan)
        {
            using (var context = ApplicationDbContext.Create())
            {
                context.Scans.Attach(scan);
                context.Entry(scan).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}