﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BadgerCloudService.Models;
using BadgerCloudService.Models.Context;
using BadgerCloudService.Objects;

namespace BadgerCloudService.Models.DAL
{
    public sealed class MachineRepository : IDisposable
    {
        ~MachineRepository()
        {
            Dispose();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public void Add(Machine machine)
        {
            using (var context = ApplicationDbContext.Create())
            {
                context.Machines.Add(machine);
                context.SaveChanges();
            }
        }

        public Machine Find(int? id)
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Machines.Find(id);
            }
        }

        public Machine FindByAlias(string alias)
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Machines.AsNoTracking()
                    .Where(m => m.Alias == alias)
                    .SingleOrDefault();
            }
        }

        internal IEnumerable<Machine> GetAll()
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Machines.AsNoTracking().ToList();
            }
        }

        internal IEnumerable<PopularMachine> PopularMachines(DateTime Start)
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Machines
                    .Select(pm => new PopularMachine()
                    {
                        Machine = pm,
                        Count = context.Scans
                            .Where(s => s.Machine_Id == pm.Id
                             && s.TimeOfScan >= Start).Count()
                    })
                    .Where(m => m.Machine.IsOnControl)
                    .OrderByDescending(pm => pm.Count).Take(10).ToList();
            }
        }
    }
}