﻿using BadgerCloudService.Objects;
using BadgerCloudService.Objects.Filters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BadgerCloudService.Models.DAL
{
    public sealed class Repository : IRepository, IDisposable
    {
        public Repository()
        {
        }

        ~Repository()
        {
            Dispose();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public Attendee AddAttendee(Attendee attendee)
        {
            using (var context = new AttendeeRepository())
            {
                context.Add(attendee);
                return attendee;
            }
        }

        public Scan AddScan(Attendee attendee, Machine machine)
        {
            var scan = new Scan()
            {
                Attendee_Id = attendee.Id,
                Machine_Id = machine.Id,
            };

            scan.TimeOfScan = DateTime.Now;
            using (var context = new ScansRepository())
            {
                context.Add(scan);
                scan = context.Find(scan.Id);
                return scan;
            }
        }

        public Machine FindMachineByAlias(string machineAlias)
        {
            using (var context = new MachineRepository())
            {
                return context.FindByAlias(machineAlias);
            }
        }

        public Event GetEventByEventCode(string eventCode)
        {
            using (var context = new EventRepository())
            {
                return context.FindEventByEventCode(eventCode);
            }
        }

        public IEnumerable<Event> GetShowListings()
        {
            using (var context = new EventRepository())
            {
                return context.GetAllEvents();
            }
        }

        public Attendee FindAttendeeByBadgeId(string badgeId)
        {
            using (var context = new AttendeeRepository())
            {
                return context.FindByBadgeId(badgeId);
            }
        }

        public Scan FindScanById(int? id)
        {
            using (var context = new ScansRepository())
            {
                return context.Find(id);
            }
        }

        public void UpdateAttendee(Attendee attendee)
        {
            using (var context = new AttendeeRepository())
            {
                context.Update(attendee);
            }
        }

        public void UpdateScan(Scan scan)
        {
            using (var context = new ScansRepository())
            {
                context.Update(scan);
            }
        }

        public IEnumerable<Scan> GetScans()
        {
            using (var context = new ScansRepository())
            {
                return context.GetAllScans();
            }
        }

        public IEnumerable<Scan> GetFilteredScansByDistributorName(string distributorName)
        {
            using (var context = new ScansRepository())
            {
                return context.GetScansByDistributorName(distributorName);
            }
        }

        public IEnumerable<Lead> GetLeadList()
        {
            using (var context = new LeadRepository())
            {
                return context.GetLeads();
            }
        }

        public Attendee FindAttendee(int? id)
        {
            using (var context = new AttendeeRepository())
            {
                return context.Find(id);
            }
        }

        public IEnumerable<Attendee> GetAttendees()
        {
            using (var context = new AttendeeRepository())
            {
                return context.GetAttendees();
            }
        }

        public IEnumerable<Attendee> GetFilteredAttendees(IFilter<Attendee> filters)
        {
            using (var context = new AttendeeRepository())
            {
                return filters.Filter(context.GetAttendees());
            }
        }

        public void RemoveAttendee(int id)
        {
            using (var context = new AttendeeRepository())
            {
                Attendee attendee = context.Find(id);
                context.Remove(attendee);
            }
        }

        public IEnumerable<Machine> GetMachines()
        {
            using (var context = new MachineRepository())
            {
                return context.GetAll();
            }
        }

        public IEnumerable<Lead> GetFilteredLeads(List<IFilter<Lead>> filters)
        {
            IEnumerable<Lead> leads = this.GetBaseListForFilteredList(filters);
            foreach (IFilter<Lead> filter in filters)
            {
                if (filter.Data == "All")
                {
                    continue;
                }
                leads = filter.Filter(leads);
            }
            return leads;
        }

        private IEnumerable<Lead> GetBaseListForFilteredList(List<IFilter<Lead>> filters)
        {
            using (var context = new LeadRepository())
            {
                IEnumerable<Lead> leads = new List<Lead>();
                if (filters.Any(f => f.FilterType == "Date"))
                {
                    var filter = filters.Where(f => f.FilterType == "Date").Single();
                    filters.Remove(filter);
                    var startDate = this.ConvertStringToDateTime(filter.Data);
                    var endDate = startDate.AddDays(1);
                    leads = context.DateFilteredLeads(startDate, endDate);
                }
                else
                {
                    leads = context.GetLeads();
                }

                return leads;
            }
        }

        private DateTime ConvertStringToDateTime(string sDate)
        {
            string[] dataArray = sDate.Split('-');
            return new DateTime(int.Parse(dataArray[0]), int.Parse(dataArray[1]), int.Parse(dataArray[2]));
        }

        public Distributor AddDistributor(Distributor distributor)
        {
            using (var context = new DistributorRepository())
            {
                return context.Add(distributor);
            }
        }

        public Distributor FindDistributor(Distributor distributor)
        {
            using (var context = new DistributorRepository())
            {
                return context.FindByNameAndBranch(distributor.Name, distributor.Branch);
            }
        }

        public Distributor FindDistributorById(int id)
        {
            using (var context = new DistributorRepository())
            {
                return context.Find(id);
            }
        }

        public StoredQuestion FindQuestionByIndex(int index)
        {
            using (var context = new QuestionRepository())
            {
                return context.FindByIndex(index);
            }
        }

        public StoredQuestion AddQuestion(QuestionXml questionXml)
        {
            StoredQuestion question = new StoredQuestion()
            {
                Index = questionXml.Index,
                Question = questionXml.Question
            };

            using (var context = new QuestionRepository())
            {
                return context.AddQuestion(question);
            }
        }

        public void AddAnswersRange(List<Answer> answers)
        {
            using (var context = new QuestionRepository())
            {
                context.AddAnswerRange(answers);
            }
        }

        public IEnumerable<StoredQuestion> GetQuestions()
        {
            using (var context = new QuestionRepository())
            {
                return context.GetQuestions();
            }
        }


        public void AddDummyEntry(string field)
        {
            using (var context = new QuestionRepository())
            {
                context.AddDummyEntry(field);
            }
        }


        public IEnumerable<PopularMachine> GetPopularMachine(DateTime dateTime)
        {
            using (var context = new MachineRepository())
            {
                return context.PopularMachines(dateTime);
            }
        }


        public int GetAttendeeCount(DateTime dateTime)
        {
            using (var context = new AttendeeRepository())
            {
                return context.CountWithDate(dateTime);
            }
        }
    }
}