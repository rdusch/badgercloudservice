﻿using BadgerCloudService.Models.Context;
using BadgerCloudService.Objects;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BadgerCloudService.Models.DAL
{
    public sealed class LeadRepository : IDisposable
    {
        ~LeadRepository()
        {
            Dispose();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Lead> GetLeads()
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Scans
                    .Include(s => s.Attendee)
                    .Include(s => s.Attendee.Address)
                    .Include(s => s.Attendee.Distributor)
                    .Include(s => s.Machine)
                    .Select(s => new Lead()
                    {
                        Id = s.Attendee.Id,
                        BadgeId =  s.Attendee.BadgeId,
                        FirstName = s.Attendee.FirstName,
                        LastName = s.Attendee.LastName,
                        Email = s.Attendee.Email,
                        PhoneNumber = s.Attendee.PhoneNumber,
                        Fax = s.Attendee.Fax,
                        Company = s.Attendee.Company,
                        Title = s.Attendee.Title,
                        Division = s.Attendee.Division,
                        Address1 = s.Attendee.Address.AddressLine1,
                        Address2 = s.Attendee.Address.AddressLine2,
                        City = s.Attendee.Address.City,
                        State = s.Attendee.Address.State,
                        PostalCode = s.Attendee.Address.PostalCode,
                        Country = s.Attendee.Address.Country,
                        Distributor = s.Attendee.Distributor.Name,
                        DistributorBranch = s.Attendee.Distributor.Branch,
                        Machine = s.Machine.Alias,
                        Notes = s.Attendee.Notes,
                        Event = s.Attendee.Event.Name,
                        VisitCount = context.Scans
                            .Where(sa => sa.Machine.ModelNumber == s.Machine.ModelNumber && sa.Attendee.Id == s.Attendee.Id)
                            .Count(),
                    }).Distinct()
                    .ToList();
            }
        }

        internal IEnumerable<Lead> DateFilteredLeads(DateTime startDate, DateTime endDate)
        {
            using (var context = ApplicationDbContext.Create())
            {
                var reportDate = startDate.Date.ToShortDateString();

                return context.Scans
                    .Include(s => s.Attendee)
                    .Include(s => s.Attendee.Address)
                    .Include(s => s.Attendee.Distributor)
                    .Include(s => s.Machine)
                    .Select(s => new Lead()
                    {
                        Id = s.Attendee.Id,
                        BadgeId = s.Attendee.BadgeId,
                        FirstName = s.Attendee.FirstName,
                        LastName = s.Attendee.LastName,
                        Email = s.Attendee.Email,
                        PhoneNumber = s.Attendee.PhoneNumber,
                        Fax = s.Attendee.Fax,
                        Company = s.Attendee.Company,
                        Title = s.Attendee.Title,
                        Division = s.Attendee.Division,
                        Address1 = s.Attendee.Address.AddressLine1,
                        Address2 = s.Attendee.Address.AddressLine2,
                        City = s.Attendee.Address.City,
                        State = s.Attendee.Address.State,
                        PostalCode = s.Attendee.Address.PostalCode,
                        Country = s.Attendee.Address.Country,
                        Distributor = s.Attendee.Distributor.Name,
                        DistributorBranch = s.Attendee.Distributor.Branch,
                        Machine = s.Machine.Alias,
                        Event = s.Attendee.Event.Name,
                        ReportDate = reportDate,
                        VisitCount = context.Scans
                            .Where(sa => sa.Machine.ModelNumber == s.Machine.ModelNumber
                                && sa.Attendee.Id == s.Attendee.Id
                                && sa.TimeOfScan >= startDate && sa.TimeOfScan < endDate)
                            .Count(),
                    }).Where(s => s.VisitCount > 0)
                    .Distinct()
                    .ToList();
            }
        }
    }
}