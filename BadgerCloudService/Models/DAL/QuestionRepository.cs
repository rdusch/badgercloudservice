﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BadgerCloudService.Models.Context;
using BadgerCloudService.Models;
using BadgerCloudService.Objects;

namespace BadgerCloudService.Models.DAL
{
    internal class QuestionRepository : IDisposable
    {
        ~QuestionRepository()
        {
            Dispose();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        internal StoredQuestion FindByIndex(int index)
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Questions.Where(q => q.Index == index).FirstOrDefault();
            }
        }

        internal StoredQuestion AddQuestion(StoredQuestion question)
        {
            using (var context = ApplicationDbContext.Create())
            {
                context.Questions.Add(question);
                context.SaveChanges();
                return question;
            }
        }

        internal void AddAnswerRange(List<Answer> answers)
        {
            using (var context = ApplicationDbContext.Create())
            {
                context.Answers.AddRange(answers);
                context.SaveChanges();
            }
        }

        internal List<StoredQuestion> GetQuestions()
        {
            using (var context = ApplicationDbContext.Create())
            {
                return context.Questions.ToList();
            }
        }

        internal void AddDummyEntry(string field)
        {
            using (var context = ApplicationDbContext.Create())
            {
                context.DummyTable.Add(new DummyTable() { Field = field });
                context.SaveChanges();
            }
        }
    }
}