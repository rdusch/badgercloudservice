namespace BadgerCloudService.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System;
    public class Attendee
    {
        public Attendee() { }

        public int Id { get; set; }
        public string BadgeId { get; set; }
        [Display(Name = "Fist Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        public string Title { get; set; }
        public string Company { get; set; }
        public string Division { get; set; }
        public int Distributor_Id { get; set; }
        [ForeignKey("Distributor_Id")]
        public Distributor Distributor { get; set; }
        public int Address_Id { get; set; }
        [ForeignKey("Address_Id")]
        public Address Address { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
        [Display(Name = "Registration Type")]
        public string RegistrationType { get; set; }
        public string Notes { get; set; }
        public int Event_Id { get; set; }
        [ForeignKey("Event_Id")]
        public Event Event { get; set; }
        public List<Answer> QuestionList = new List<Answer>();
        public DateTime DateAdded { get; set; }

        public string GetPostalCode()
        {
            return this.Address.PostalCode;
        }
    }
}