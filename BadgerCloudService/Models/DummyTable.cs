﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BadgerCloudService.Models
{
    public class DummyTable
    {
        public int Id { get; set; }
        public string Field { get; set; }
    }
}