﻿using System.ComponentModel.DataAnnotations;
namespace BadgerCloudService.Models
{
    public class Machine
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Name")]
        public string Alias { get; set; }
        public string SerialNumber { get; set; }
        [Display(Name = "Machine")]
        public string ModelNumber { get; set; }
        public bool IsOnControl { get; set; }
    }
}