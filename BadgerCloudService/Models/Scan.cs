﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace BadgerCloudService.Models
{
    public class Scan
    {
        public int Id { get; set; }
        public int Attendee_Id { get; set; }
        [ForeignKey("Attendee_Id")]
        public Attendee Attendee { get; set; }
        public int Machine_Id { get; set; }
        [ForeignKey("Machine_Id")]
        public Machine Machine { get; set; }

        [Display(Name = "Time Of Scan")]
        public DateTime TimeOfScan { get; set; }
    }
}