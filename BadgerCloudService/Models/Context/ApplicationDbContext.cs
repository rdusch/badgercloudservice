﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace BadgerCloudService.Models.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
            Database.SetInitializer<ApplicationDbContext>(new CreateDatabaseIfNotExists<ApplicationDbContext>());
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = false;

        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Address> Addresses { get; set; }
        public DbSet<Attendee> Attendees { get; set; }
        public DbSet<Machine> Machines { get; set; }
        public DbSet<Scan> Scans { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Distributor> Distributors { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<StoredQuestion> Questions { get; set; }
        public DbSet<DummyTable> DummyTable { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}