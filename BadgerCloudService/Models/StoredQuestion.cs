﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BadgerCloudService.Models
{
    public class StoredQuestion
    {
        public int Id { get; set; }
        public int Index { get; set; }
        public string Question { get; set; }
        public bool Active { get; set; }
    }
}