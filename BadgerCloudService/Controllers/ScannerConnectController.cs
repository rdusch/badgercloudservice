﻿using BadgerCloudService.Models;
using BadgerCloudService.Models.DAL;
using BadgerCloudService.Objects;
using BadgerCloudService.Services.DistributorSources;
using BadgerCloudService.Services.EventSources;
using BadgerCloudService.Services.EventSources.IMTS;
using BadgerCloudService.ViewModels;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using System.Linq;

namespace BadgerCloudService.Controllers
{
    public class ScannerConnectController : Controller
    {
        private readonly IEventService _eventHost;
        private readonly IDistributorList _distributors;
        private IRepository _repo;
        
        public ScannerConnectController()
        {
            this._eventHost = new EventHostService();
            this._distributors = new DistributorList();
            this._repo = new Repository();
        }

        public ScannerConnectController(IEventService eventHost, IDistributorList distributors, IRepository repo)
        {
            this._eventHost = eventHost;
            this._distributors = distributors;
            this._repo = repo;
        }

        [HttpPost]
        public JsonResult LeadLookUp([Bind(Include = "Token, ScanData, LocationAlias")]PostViewModel vm)
        {
            try
            {
                if (ModelState.IsValid && (vm.Token == SettingConfig.GetSetting("accessToken")))
                {
                    var scan = this._repo.AddScan(this.GatherAttendeeInfo(vm.LeadLookUp), this._repo.FindMachineByAlias(vm.LocationAlias));
                    scan.Attendee.Distributor = this._repo.FindDistributorById(scan.Attendee.Distributor_Id);
                    Response.StatusCode = (int)HttpStatusCode.OK;
                    return Json(scan.Attendee);
                }
            }
            catch( Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(new { Message = ex.Message });
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(new { Message = "Failed Look Up" });
        }

        [HttpPost]
        public JsonResult GetShowList(string Token)
        {
            try
            {
                if (ModelState.IsValid && (Token == SettingConfig.GetSetting("accessToken")))
                {
                    var showList = this._repo.GetShowListings();
                    Response.StatusCode = (int)HttpStatusCode.OK;
                    return Json(new { showList });
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(new { Message = ex.Message });
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(new { Message = "Failed to Retrieve Show List" });
        }

        [HttpPost]
        public JsonResult UpdateAttendee([Bind(Include = "Token, ScanData, Comments, QuestionList")]PostViewModel vm)
        {
            try
            {
                if (ModelState.IsValid && (vm.Token == SettingConfig.GetSetting("accessToken")))
                {
                    var attendee = this.GatherAttendeeInfo(vm.LeadLookUp);
                    if (vm.Comments != null && vm.Comments != string.Empty)
                    {
                        attendee.Notes += vm.Comments + ", ";
                    }

                    this._repo.AddDummyEntry(vm.QuestionList);

                    if (vm.QuestionListXml != null && vm.QuestionList != string.Empty)
                    {
                        AddAttendeeAnswers(attendee, vm.QuestionListXml);
                    }

                    this._repo.UpdateAttendee(attendee);
                    Response.StatusCode = (int)HttpStatusCode.OK;
                    return Json(true);
                }
            }
            catch(Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(new { Message = ex.Message });
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(false);
        }

        public JsonResult AddAttendee([Bind(Include = "Token, LastName, BadgeId")]AddAttendeeVM vm)
        {
            try
            {
                if (ModelState.IsValid && (vm.Token == SettingConfig.GetSetting("accessToken")))
                {
                    var attendee = this.GatherAttendeeInfo(vm.LookUp);
                    return Json(attendee);
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(new { Message = ex.Message });
            }

            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json(false);
        }

        private void AddAttendeeAnswers(Attendee attendee, List<QuestionXml> list)
        {
            List<StoredQuestion> questions = GetQuestions(list);
            List<Answer> answers = new List<Answer>();
            foreach(QuestionXml thing in list)
            {
                answers.Add(new Answer()
                {
                    Attendee_Id = attendee.Id,
                    Question_Id = questions.Where(q => q.Index == thing.Index).FirstOrDefault().Id,
                    AttendeeAnswer = thing.Answer
                });
            }

            this._repo.AddAnswersRange(answers);
        }

        private List<StoredQuestion> GetQuestions(List<QuestionXml> list)
        {
            List<StoredQuestion> questions = new List<StoredQuestion>();
            foreach (QuestionXml questionXml in list)
            {
                StoredQuestion question = this._repo.FindQuestionByIndex(questionXml.Index);
                if (question == null)
                {
                    question = this._repo.AddQuestion(questionXml);
                    questions.Add(question);
                }
                else
                {
                    questions.Add(question);
                }
            }

            return questions;
        }

        private Answer CreateAnswer(Attendee attendee, StoredQuestion question)
        {
            Answer answer = new Answer()
            {
                Attendee_Id = attendee.Id,
                Question_Id = question.Id,
                AttendeeAnswer = question.Question
            };
            return answer;
        }

        private Attendee GatherAttendeeInfo(Lookup lookup)
        {
            var attendee = this.FindOrCreateAttendee(lookup);
            return attendee;
        }

        private Attendee FindOrCreateAttendee(Lookup lookup)
        {
            try
            {
                Attendee attendee = this._repo.FindAttendeeByBadgeId(lookup.BadgeId);
                if (attendee == null)
                {
                    var auth = this._repo.GetEventByEventCode(lookup.EventCode);
                    attendee = this._eventHost.LookUpEventParticipant(lookup, auth);
                    attendee.BadgeId = lookup.BadgeId;
                    attendee.Event_Id = auth.Id;
                    attendee.Distributor_Id = this.FindOrCreateDistributor(attendee).Id;
                    attendee.DateAdded = DateTime.Now;
                    attendee = this._repo.AddAttendee(attendee);
                }

                return attendee;
            }
            catch(Exception ex)
            {
                throw new Exception("Unable to find Attendee with Badge Id of "+ lookup.BadgeId + " " + ex.Message, ex);
            }
        }

        private Distributor FindOrCreateDistributor(Attendee attendee)
        {
            try
            {
                var temp = this._distributors.FindDistributor(attendee.Address);
                Distributor distributor = this._repo.FindDistributor(temp);
                if (distributor == null)
                {
                    distributor = this._repo.AddDistributor(temp);
                }

                return distributor;
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to locate Distributor for attendee with Badge Id of " + attendee.BadgeId + " " + ex.Message, ex);
            }
        }
    }
}