﻿using BadgerCloudService.Library;
using BadgerCloudService.Models;
using BadgerCloudService.Models.DAL;
using BadgerCloudService.Services.DistributorSources;
using BadgerCloudService.Services.EventSources;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;

namespace BadgerCloudService.Controllers
{
    public class ScansController : Controller
    {
        private IRepository _repo;
        private IDistributorList _distributors;

        public ScansController()
        {
            this._repo = new Repository();
            this._distributors = new DistributorList();
        }

        public ScansController(IRepository repo, IDistributorList distributors)
        {
            this._repo = repo;
            this._distributors = distributors;
        }

        public ActionResult Index()
        {
            return View(_repo.GetScans());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Scan scan = _repo.FindScanById(id);
            if (scan == null)
            {
                return HttpNotFound();
            }
            return View(scan);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Scan scan = _repo.FindScanById(id);
            if (scan == null)
            {
                return HttpNotFound();
            }
            return View(scan);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TimeOfScan")] Scan scan)
        {
            if (ModelState.IsValid)
            {
                _repo.UpdateScan(scan);
                return RedirectToAction("Index");
            }
            return View(scan);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Scan scan = _repo.FindScanById(id);
            if (scan == null)
            {
                return HttpNotFound();
            }
            return View(scan);
        }

        [HttpPost]
        public ActionResult GetFilteredList(string name)
        {
            ViewBag.Distributors = this.GetDistributorNameList();
            IEnumerable<Scan> scans = new List<Scan>();
            if (name == "Select a Distributor")
            {
                scans = this._repo.GetScans();
            }
            else
            {
                scans = this._repo.GetFilteredScansByDistributorName(name);
            }
            
            Response.StatusCode = (int)HttpStatusCode.OK;
            return PartialView("Templates/Scans/ScanList", scans);
        }

        public ActionResult Create()
        {
            return View();
        }

        private List<SelectListItem> GetDistributorNameList()
        {
            return DistributorDropDownContents.GetDistributorNameList(this._distributors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
