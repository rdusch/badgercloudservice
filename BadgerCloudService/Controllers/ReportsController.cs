﻿using BadgerCloudService.Models.DAL;
using BadgerCloudService.Objects;
using BadgerCloudService.Objects.Filters.Leads;
using BadgerCloudService.Objects.Filters;
using BadgerCloudService.Services.ReportService;
using BadgerCloudService.ViewModels;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;

namespace BadgerCloudService.Controllers
{
    public class ReportsController : Controller
    {
        private readonly string REPORT_LOCATION = SettingConfig.GetSetting("ReportLocation");
        private IRepository _repo;
        private IReportService _report;

        public ReportsController() : this(new Repository(), new ExcelWriter()) { }

        public ReportsController(IRepository repo, IReportService report)
        {
            this._repo = new Repository();
            this._report = new ExcelWriter();
        }

        [HttpPost]
        public JsonResult GetExcelExport(FilterViewModel[] filtersVm)
        {
            List<IFilter<Lead>> filters = LeadFilterFactory.CreateFilters(filtersVm);
            IEnumerable<Lead> leads = this._repo.GetFilteredLeads(filters);
            var package = this._report.CreateWorkBook(leads);
            var fileName = "Leads.xls";
            if (!Directory.Exists(Server.MapPath(REPORT_LOCATION)))
            {
                Directory.CreateDirectory(Server.MapPath(REPORT_LOCATION));
            }

            string fullPath = Path.Combine(Server.MapPath(REPORT_LOCATION), fileName);
            package.SaveAs(new FileInfo(fullPath));
            return Json(fileName);
        }

        public ActionResult DownLoad(string fileName)
        {
            string fullPath = Path.Combine(Server.MapPath(REPORT_LOCATION), fileName);
            var file = File(fullPath, "application/vnd.ms-excel", fileName);
            return file;
        }
    }
}