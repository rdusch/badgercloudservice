﻿using BadgerCloudService.Services.DistributorSources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BadgerCloudService.Library;
using BadgerCloudService.Models.DAL;

namespace BadgerCloudService.Controllers
{
    public class FiltersController : Controller
    {
        private IDistributorList _distributors = new DistributorList();
        private IRepository _repo = new Repository(); 

        // GET: Filters
        public ActionResult GetFilter(string filter, string parameter)
        {
            string viewFile = string.Empty;
            switch (filter)
            {
                case ("DateFilter"):
                    viewFile = "Templates/Filters/DateFilter";
                    break;
                case ("MachineFilter"):
                    ViewBag.Machines = MachineDropDownListContents.GetMachineNameListContents(this._repo);
                    viewFile = "Templates/Filters/MachineFilter";
                    break;
                case ("DistributorFilter"):
                    ViewBag.Distributors = DropDownContentsWrapper.GetList(DistributorDropDownContents.GetDistributorNameList(this._distributors), "Select a Distributor");
                    viewFile = "Templates/Filters/DistributorFilter";
                    break;
                case ("NameFilter") :
                    viewFile = "Templates/Filters/NameFilter";
                    break;
                case ("EventFilter"):
                    ViewBag.Events = EventDropDownContents.GetEventContents(this._repo);
                    viewFile = "Templates/Filters/EventFilter";
                    break;
                case ("BranchFilter"):
                    ViewBag.Branches = DropDownContentsWrapper.GetList(DistributorDropDownContents.GetDistributorBranchesByDistributor(this._distributors, parameter), "Select a Branch");
                    viewFile = "Templates/Filters/DistributorBranchFilter";
                    break;
            }

            return PartialView(viewFile);
        }
    }
}