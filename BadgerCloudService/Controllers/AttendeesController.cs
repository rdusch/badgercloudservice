﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BadgerCloudService.Models;
using BadgerCloudService.Models.DAL;
using BadgerCloudService.Models.Context;
using BadgerCloudService.Objects.Filters;
using BadgerCloudService.Objects.Filters.AttendeeFilters;

namespace BadgerCloudService.Controllers
{
    public class AttendeesController : Controller
    {
        private IRepository db = new Repository();

        // GET: Attendees
        public ActionResult Index()
        {
            return View(db.GetAttendees());
        }

        [HttpPost]
        public ActionResult Search(string Data)
        {
            IFilter<Attendee> filter = new NameFilter(Data);
            return PartialView("Templates/Attendees/AttendeesTable", db.GetFilteredAttendees(filter));
        }

        // GET: Attendees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendee attendee = db.FindAttendee(id);
            if (attendee == null)
            {
                return HttpNotFound();
            }

            ViewBag.Questions = this.db.GetQuestions();
            return View(attendee);
        }

        // GET: Attendees/Create
        public ActionResult Create()
        {
            return View();
        }

        // GET: Attendees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendee attendee = db.FindAttendee(id);
            if (attendee == null)
            {
                return HttpNotFound();
            }
            return View(attendee);
        }

        // POST: Attendees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,BadgeId,FirstName,LastName,Title,Company,Division,Email,PhoneNumber,Fax,RegistrationType,Notes,Address_Id,Event_Id,Distributor_Id")] Attendee attendee)
        {
            if (ModelState.IsValid)
            {
                db.UpdateAttendee(attendee);
                return RedirectToAction("Index");
            }
            return View(attendee);
        }

        // GET: Attendees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendee attendee = db.FindAttendee(id);
            if (attendee == null)
            {
                return HttpNotFound();
            }
            return View(attendee);
        }

        // POST: Attendees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            db.RemoveAttendee(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
