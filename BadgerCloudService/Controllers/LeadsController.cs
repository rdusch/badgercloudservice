﻿using BadgerCloudService.Library;
using BadgerCloudService.Models.DAL;
using BadgerCloudService.Objects;
using BadgerCloudService.Objects.Filters.Leads;
using BadgerCloudService.Objects.Filters;
using BadgerCloudService.Services.DistributorSources;
using BadgerCloudService.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace BadgerCloudService.Controllers
{
    public class LeadsController : Controller
    {
        private IRepository _repo;
        private IDistributorList _distributors;

        public LeadsController()
        {
            this._repo = new Repository();
            this._distributors = new DistributorList();
        }

        public LeadsController(IRepository repo, IDistributorList distributorList)
        {
            this._repo = repo;
            this._distributors = distributorList;
        }

        // GET: Leads
        public ActionResult Index()
        {
            this.ViewBag.Distributors = this.GetDistributorNameList();
            this.ViewBag.Machines = this.GetMachineList();
            return View(this._repo.GetLeadList());
        }

        public ActionResult GetList(FilterViewModel[] filtersVm)
        {
            List<IFilter<Lead>> filters = LeadFilterFactory.CreateFilters(filtersVm);
            IEnumerable<Lead> leads = this._repo.GetFilteredLeads(filters);
            return PartialView("Templates/Leads/LeadsList", leads);
        }

        private List<SelectListItem> GetMachineList()
        {
            return MachineDropDownListContents.GetMachineNameListContents(this._repo);
        }

        private List<SelectListItem> GetDistributorNameList()
        {
            return DistributorDropDownContents.GetDistributorNameList(this._distributors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}