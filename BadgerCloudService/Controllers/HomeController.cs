﻿namespace BadgerCloudService.Controllers
{
    using BadgerCloudService.Models.DAL;
    using BadgerCloudService.Objects;
    using BadgerCloudService.Objects.Filters.Leads;
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;

    public class HomeController : Controller
    {
        private IRepository _repo;

        public HomeController()
        {
            this._repo = new Repository();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LeadsTodayCount()
        {
            this.ViewBag.Header = "Leads Today";
            this.ViewBag.Count = this.GetLeadsTodayCount();
            return PartialView("LeadsTotals");
        }

        public ActionResult LeadsTotalCount()
        {
            this.ViewBag.Header = "Leads Total";
            this.ViewBag.Count = this.GetLeadsTotalCount();
            return PartialView("LeadsTotals");
        }

        public ActionResult PopularMachinesToday()
        {
            this.ViewBag.Header = "Most Popular Machines Of Today";
            return PartialView("PopularMachines", this.GetPopularMachinesToday());
        }

        public ActionResult PopularMachineTotal()
        {
            this.ViewBag.Header = "Most Popular Machines Of All Time";
            return PartialView("PopularMachines", this.GetPopularMachinesTotal());
        }

        private List<PopularMachine> GetPopularMachinesTotal()
        {
            return this._repo.GetPopularMachine(new DateTime().Date).ToList();
        }

        private List<PopularMachine> GetPopularMachinesToday()
        {
            return this._repo.GetPopularMachine(DateTime.Now.Date).ToList();
        }

        private int GetLeadsTotalCount()
        {
            var leads = this._repo.GetLeadList();
            int count = 0;
            foreach (string lead in leads.Select(l => l.BadgeId).Distinct())
            {
                count++;
            }

            return count;
        }

        private int GetLeadsTodayCount()
        {
            var date = DateTime.Now.Date.ToString();


            var filter = new DateFilter(datesAreDumb(DateTime.Now.Date));
            var filterList = new List<Objects.Filters.IFilter<Lead>>();
            filterList.Add(filter);
            var leads = this._repo.GetFilteredLeads(filterList);
            int count = 0;
            foreach (string lead in leads.Select(l => l.BadgeId).Distinct())
            {
                count++;
            }
            return count;
        }

        private string datesAreDumb(DateTime date)
        {
            return string.Format("{0}-{1}-{2}", date.Year, date.Month, date.Day);
        }
    }
}