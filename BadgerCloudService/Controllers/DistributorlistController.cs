﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace BadgerCloudService.Controllers
{
    public class DistributorListController : Controller
    {
        private readonly string DistributorLocation = SettingConfig.GetSetting("DistributorLocation");
        private readonly string DistributorFileName = SettingConfig.GetSetting("DistributorFileName");

        public ActionResult Index()
        {
            return View();
        } 

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase file)
        {
            if (!Directory.Exists(Server.MapPath(this.DistributorLocation)))
            {
                Directory.CreateDirectory(Server.MapPath(this.DistributorLocation));
            }

            string path = System.IO.Path.Combine(Server.MapPath(this.DistributorLocation), System.IO.Path.GetFileName(this.DistributorFileName));
            file.SaveAs(path);
            ViewBag.Message = "File uploaded successfully";
            return View();
        }
    }
}