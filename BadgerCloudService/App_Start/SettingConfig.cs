﻿using System.Web.Configuration;

namespace BadgerCloudService
{
    public class SettingConfig
    {
        public static string GetSetting(string requestedSetting)
        {
            System.Configuration.Configuration rootWebConfig = WebConfigurationManager.OpenWebConfiguration("\\Web.config");
            if (rootWebConfig.AppSettings.Settings.Count > 0)
            {
                System.Configuration.KeyValueConfigurationElement setting = rootWebConfig.AppSettings.Settings[requestedSetting];
                if (setting != null)
                {
                    return setting.Value;
                }
                else
                {
                    return string.Empty;
                }
            }

            return string.Empty;
        }
    }
}