﻿using BadgerCloudService.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BadgerCloudService.Tests.Models
{
    public class EventParticipantTests
    {
        private Attendee sut;

        [TestInitialize]
        public void SetUp()
        {
            this.sut = new Attendee();
        }

        [TestMethod]
        public void GetPostalCode()
        {
            var expected = "12345";

            this.sut.Address = new Address()
            {
                PostalCode = expected
            };

            Assert.AreEqual(expected, this.sut.GetPostalCode());
        }
    }
}
