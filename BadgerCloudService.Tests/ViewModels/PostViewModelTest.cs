﻿using BadgerCloudService.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BadgerCloudService.Tests.ViewModels
{
    [TestClass]
    public class PostViewModelTest
    {
        [TestMethod]
        public void CreatLookUpObject()
        {
            PostViewModel sut = new PostViewModel();

            sut.ScanData = "200001$ShowCode$FirstName$LastName$Company$";

            Assert.AreEqual("200001", sut.LeadLookUp.BadgeId);
            Assert.AreEqual("ShowCode", sut.LeadLookUp.EventCode);
            Assert.AreEqual("FirstName", sut.LeadLookUp.FirstName);
            Assert.AreEqual("LastName", sut.LeadLookUp.LastName);
            Assert.AreEqual("Company", sut.LeadLookUp.Company);
        }
    }
}
