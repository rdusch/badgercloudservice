﻿using BadgerCloudService.Models;
using BadgerCloudService.Services.DistributorSources;
using BadgerCloudService.Services.DistributorSources.Readers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BadgerCloudService.Tests.Services.DistributorSource
{
    public class DistributorListTests
    {
        private DistributorList sut;
        private Mock<IReader> mockReader = new Mock<IReader>();

        [TestInitialize]
        public void Setup()
        {
            this.sut = new DistributorList(mockReader.Object);
        }

        [TestCleanup]
        public void TearDown()
        {
            this.mockReader.VerifyAll();
        }

        [TestMethod]
        public void FindDistributerForParticipant_USDistributor()
        {
            var expected = new Distributor()
            {
                Name = "MORRIS",
                Branch = ""
            };

            Address lookup = new Address()
            {
                Country = "UNITED STATES",
                PostalCode = "27376"
            };

            this.mockReader.Setup(r => r.FindByPostalCode(lookup.PostalCode)).Returns(expected);

            var actual = this.sut.FindDistributor(lookup);

            Assert.AreEqual("MORRIS", actual);
        }

        [TestMethod]
        public void FindDistributorForParticipant_CanadaDistributor()
        {
            var expected = new Distributor()
            {
                Name = "THOMAS SKINNER",
                Branch = ""
            };

            Address lookup = new Address()
            {
                Country = "CANADA",
                PostalCode = "T0A"
            };

            this.mockReader.Setup(r => r.FindByPostalCode(lookup.PostalCode)).Returns(expected);

            var actual = this.sut.FindDistributor(lookup);

            Assert.AreEqual("THOMAS SKINNER", actual);
        }

        [TestMethod]
        public void FindDistributorForParticipant_OutsideUsAndCanada()
        {
            var expected = new Distributor()
            {
                Name = "OEG",
                Branch = ""
            };

            Address lookup = new Address()
            {
                Country = "ITALY",
                PostalCode = ""
            };

            this.mockReader.Setup(r => r.FindByCountryCode(lookup.Country)).Returns(expected);

            var actual = this.sut.FindDistributor(lookup);

            Assert.AreEqual("OEG", actual);
        }
    }
}
