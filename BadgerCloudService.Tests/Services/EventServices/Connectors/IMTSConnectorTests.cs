﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BadgerCloudService.Models;
using BadgerCloudService.Objects;
using BadgerCloudService.Services.EventSources.IMTS.Connectors.IMTS2016;

namespace BadgerCloudService.Tests.Services.EventServices.Connectors
{
    [TestClass]
    public class IMTSConnectorTests
    {
        private IMTSConnection sut;

        [TestInitialize]
        public void Setup()
        {
            this.sut = new IMTSConnection();
        }

        [TestCleanup]
        public void TearDown()
        {

        }

        [TestMethod]
        public void GetAttendeeData()
        {
            Lookup lookup = new Lookup()
            {
                BadgeId = "200000",
                LastName = "Last"
            };

            Event eventModel = new Event()
            {
                URl = "https://www.xpressleadpro.com/Leads/LeadsAPI/LeadsAPI.php",
                AuthKey = "92EF378E-AE73-46DB-9848-102ECBB2639E",
                ExhibitId = "1735750",
                EventCode = "IMTS0916"
            };

            var actual = this.sut.FindByBadgeIdAndLastName(lookup, eventModel);

            Assert.AreEqual("First", actual.FirstName);
        }
    }
}
