﻿using BadgerCloudService.Controllers;
using BadgerCloudService.Models;
using BadgerCloudService.Models.DAL;
using BadgerCloudService.Services.DistributorSources;
using BadgerCloudService.Services.EventSources;
using BadgerCloudService.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace BadgerCloudService.Tests.Controllers
{
    [TestClass]
    public class ScannerConnectControllerTests
    {
        #region setup
        private ScannerConnectController sut;
        private Mock<IDistributorList> mockDistributor = new Mock<IDistributorList>();
        private Mock<IEventService> mockEventHost = new Mock<IEventService>();
        private Mock<IRepository> mockRepo = new Mock<IRepository>();
        private Mock<HttpContextBase> mockHttpContext = new Mock<HttpContextBase>();
        private Mock<HttpResponseBase> mockHttpResponse = new Mock<HttpResponseBase>();
        private JsonResult expected;
        private string AccessToken = "BadgerToken";

        [TestInitialize]
        public void SetUp()
        {
            this.sut = new ScannerConnectController(this.mockEventHost.Object, this.mockDistributor.Object, this.mockRepo.Object)
            {
                ControllerContext = new System.Web.Mvc.ControllerContext()
                {
                    HttpContext = mockHttpContext.Object
                }
            };

            this.expected = new JsonResult();
        }

        [TestCleanup]
        public void TearDown()
        {
            this.mockDistributor.VerifyAll();
            this.mockEventHost.VerifyAll();
            this.mockHttpContext.VerifyAll();
            this.mockHttpResponse.VerifyAll();
            this.mockRepo.VerifyAll();
        }

        [TestMethod]
        public void TestCreate()
        {
            Assert.IsNotNull(this.sut);
        }
        #endregion

        #region LeadLookUp
        [TestMethod]
        public void LeadLookUp_FindLeadWithDistributerInfo_ExistingRecords()
        {
            var vm = new PostViewModel()
            {
                Token = this.AccessToken,
                ScanData = "200001$ShowCode$FirstName$LastName$Company$",
                LocationAlias = "12345678980"
            };

            var distributor = new Distributor()
            {
                Name = "MORRIS",
                Branch = ""
            };

            var attendee = new Attendee()
            {
                Id = 1,
                BadgeId = vm.LeadLookUp.BadgeId,
                Distributor = distributor,
                Address = new Address()
                {
                    PostalCode = "12345",
                    Country = "UNITED STATES"
                }
            };

            var machine = new Machine();

            var scan = new Scan()
            {
                Attendee = attendee,
                Machine = machine
            };

            this.expected.Data = attendee;

            this.mockHttpContext.SetupGet(hc => hc.Response).Returns(mockHttpResponse.Object).Verifiable();
            
            this.mockRepo.Setup(r => r.FindAttendeeByBadgeId(vm.LeadLookUp.BadgeId)).Returns(attendee).Verifiable();

            this.mockDistributor.Setup(d => d.FindDistributor(attendee.Address)).Returns(distributor).Verifiable();

            this.mockRepo.Setup(r => r.FindMachineByAlias(vm.LocationAlias)).Returns(machine).Verifiable();
            this.mockRepo.Setup(r => r.AddScan(attendee, machine)).Returns(scan);

            this.mockHttpResponse.SetupSet(hr => hr.StatusCode = (int)HttpStatusCode.OK).Verifiable();

            var actual = this.sut.LeadLookUp(vm);

            Assert.AreEqual(expected.Data, actual.Data);
        }

        [TestMethod]
        public void LeadLookUp_FindLeadWithDistributorInfo_CreateRecords()
        {
            var vm = new PostViewModel()
            {
                Token = this.AccessToken,
                ScanData = "200001$ShowCode$FirstName$LastName$Company$",
                LocationAlias = "1234567890"
            };

            var distributor = new Distributor()
            {
                Name = "MORRIS",
                Branch = ""
            };

            var attendee = new Attendee()
            {
                BadgeId = vm.LeadLookUp.BadgeId,
                Distributor = distributor,
                Address = new Address()
                {
                    PostalCode = "12345",
                    Country = "UNITED STATES"
                }
            };

            var savedAttendee = new Attendee()
            {
                Id = 123,
                BadgeId = vm.LeadLookUp.BadgeId,
                Address = new Address()
                {
                    PostalCode = "12345",
                    Country = "UNITED STATES"
                }
            };

            Event eventModel = new Event();

            var ExpectedAttendee = savedAttendee;
            ExpectedAttendee.Distributor = distributor;

            var machine = new Machine();

            var scan = new Scan()
            {
                Attendee = ExpectedAttendee,
                Machine = machine
            };

            this.expected.Data = ExpectedAttendee;

            this.mockHttpContext.SetupGet(hc => hc.Response).Returns(mockHttpResponse.Object).Verifiable();

            this.mockRepo.Setup(r => r.FindAttendeeByBadgeId(vm.LeadLookUp.BadgeId)).Verifiable();
            this.mockRepo.Setup(r => r.GetEventByEventCode(vm.LeadLookUp.EventCode)).Returns(eventModel).Verifiable();
            this.mockEventHost.Setup(r => r.LookUpEventParticipant(vm.LeadLookUp, eventModel)).Returns(attendee).Verifiable();
            this.mockRepo.Setup(r => r.AddAttendee(attendee)).Returns(savedAttendee).Verifiable();

            this.mockDistributor.Setup(d => d.FindDistributor(savedAttendee.Address)).Returns(distributor).Verifiable();
            this.mockRepo.Setup(r => r.FindMachineByAlias(vm.LocationAlias)).Returns(machine);
            this.mockRepo.Setup(r => r.AddScan(ExpectedAttendee, machine)).Returns(scan);

            this.mockHttpResponse.SetupSet(hr => hr.StatusCode = (int)HttpStatusCode.OK).Verifiable();

            var actual = this.sut.LeadLookUp(vm);

            Assert.AreEqual(expected.Data, actual.Data);
        }

        [TestMethod]
        public void LeadLookUp_FailedAttendeeLookup()
        {
            var vm = new PostViewModel()
            {
                Token = this.AccessToken,
                ScanData = "200001$ShowCode$FirstName$LastName$Company$",
                LocationAlias = "1234567890"
            };

            var exceptionMessage = "Unable to find Attendee with Badge Id of " + vm.LeadLookUp.BadgeId;

            this.expected.Data = new { Message = exceptionMessage };

            this.mockHttpContext.SetupGet(hc => hc.Response).Returns(mockHttpResponse.Object).Verifiable();
            this.mockRepo.Setup(r => r.FindAttendeeByBadgeId(vm.LeadLookUp.BadgeId)).Throws(new Exception(exceptionMessage)).Verifiable();
            this.mockHttpResponse.SetupSet(hr => hr.StatusCode = (int)HttpStatusCode.BadRequest).Verifiable();

            var actual = this.sut.LeadLookUp(vm);
            var expectedString = expected.Data.ToString();
            var actualString = actual.Data.ToString();

            Assert.AreEqual(expectedString, actualString);
        }

        [TestMethod]
        public void LeadLookUp_FailedDistributorLookup()
        {
            var vm = new PostViewModel()
            {
                Token = this.AccessToken,
                ScanData = "200001$ShowCode$FirstName$LastName$Company$"
            };
            
            var attendee = new Attendee()
            {
                Id = 0,
                BadgeId = vm.LeadLookUp.BadgeId,
                Distributor = null,
                Address = new Address()
                {
                    PostalCode = "12345"
                }
            };

            var exceptionMessage = "Unable to locate Distributor for attendee with Badge Id of " + attendee.BadgeId;
            this.expected.Data = new { Message = exceptionMessage };

            this.mockHttpContext.SetupGet(hc => hc.Response).Returns(mockHttpResponse.Object).Verifiable();
            this.mockRepo.Setup(r => r.FindAttendeeByBadgeId(vm.LeadLookUp.BadgeId)).Returns(attendee).Verifiable();
            this.mockDistributor.Setup(d => d.FindDistributor(attendee.Address)).Throws(new Exception(exceptionMessage)).Verifiable();
            this.mockHttpResponse.SetupSet(hr => hr.StatusCode = (int)HttpStatusCode.BadRequest).Verifiable();

            var actual = this.sut.LeadLookUp(vm);
            var expectedString = expected.Data.ToString();
            var actualString = actual.Data.ToString();

            Assert.AreEqual(expectedString, actualString);
        }

        [TestMethod]
        public void LeadLookUp_ModelStateInvalid()
        {
            this.sut.ModelState.AddModelError("key", "Failed Magically");
            var message = "Failed Look Up";
            var vm = new PostViewModel()
            {
                Token = this.AccessToken,
                ScanData = "200001$ShowCode$FirstName$LastName$Company$"
            };

            this.expected.Data = new { Message = message };

            this.mockHttpContext.SetupGet(hc => hc.Response).Returns(mockHttpResponse.Object);
            this.mockHttpResponse.SetupSet(hr => hr.StatusCode = (int)HttpStatusCode.BadRequest);

            var actual = this.sut.LeadLookUp(vm);

            Assert.AreEqual(expected.Data.ToString(), actual.Data.ToString());
        }
        #endregion

        #region GetShowList
        [TestMethod]
        public void GetShowList()
        {
            var showList = new List<Event>();
            showList.Add(new Event()
            {
                Id = 0
            });
            showList.Add(new Event()
            {
                Id = 1
            });

            this.expected.Data = new { showlist = showList };

            this.mockHttpContext.SetupGet(hc => hc.Response).Returns(mockHttpResponse.Object).Verifiable();
            this.mockRepo.Setup(r => r.GetShowListings()).Returns(showList).Verifiable();
            this.mockHttpResponse.SetupSet(hr => hr.StatusCode = (int)HttpStatusCode.OK).Verifiable();

            var actual = this.sut.GetShowList(this.AccessToken);

            Assert.AreEqual(this.expected.Data.ToString(), actual.Data.ToString());
        }

        [TestMethod]
        public void GetShowList_ModelStateNotValid()
        {
            this.expected.Data = new { Message = "Failed to Retrieve Show List" };

            this.sut.ModelState.AddModelError("key", "Failed Magically");

            this.mockHttpContext.SetupGet(hc => hc.Response).Returns(mockHttpResponse.Object).Verifiable();
            this.mockHttpResponse.SetupSet(hr => hr.StatusCode = (int)HttpStatusCode.BadRequest).Verifiable();

            var actual = this.sut.GetShowList(this.AccessToken);

            Assert.AreEqual(this.expected.Data.ToString(), actual.Data.ToString());
        }

        [TestMethod]
        public void GetShowList_ExcpetionThrownFromEventHost()
        {
            string message = "thingy is brokey";

            this.expected.Data = new { Message = message };

            this.mockHttpContext.SetupGet(hc => hc.Response).Returns(mockHttpResponse.Object).Verifiable();
            this.mockRepo.Setup(r => r.GetShowListings()).Throws(new Exception(message)).Verifiable();
            this.mockHttpResponse.SetupSet(hr => hr.StatusCode = (int)HttpStatusCode.BadRequest).Verifiable();

            var actual = this.sut.GetShowList(this.AccessToken);

            Assert.AreEqual(this.expected.Data.ToString(), actual.Data.ToString());
        }
        #endregion
    }
}
