﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BadgerCloudService.Library;
using BadgerCloudService.Objects;

namespace BadgerCloudService.Tests.Includes
{
    [TestClass]
    public class ScanParserTest
    {
        [TestMethod]
        public void ParseScanData()
        {
            ScanParser sut = new ScanParser();
            string rawData = "BadgeId$ShowCode$FirstName$LastName$Company$";
            var parsedData = sut.ParseData(rawData);

            Assert.IsInstanceOfType(parsedData, typeof(Lookup));
            Assert.AreEqual("BadgeId", parsedData.BadgeId);
        }
    }
}
